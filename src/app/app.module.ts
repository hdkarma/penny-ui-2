import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderMainComponent } from './common/header-main/header-main.component';
import { FooterMainComponent } from './common/footer-main/footer-main.component';
import { HomeMainComponent } from './pages/home-main/home-main.component';
import { CategoriesListComponent } from './components/categories-list/categories-list.component';
import { FeaturedProductsComponent } from './components/featured-products/featured-products.component';
import { OfferSliderComponent } from './components/offer-slider/offer-slider.component';
import { SupplierStoresComponent } from './components/supplier-stores/supplier-stores.component';
import { TopBrandsComponent } from './components/top-brands/top-brands.component';
import { SearchBlockComponent } from './components/search-block/search-block.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatButtonModule} from '@angular/material/button';
import {MatSliderModule} from '@angular/material/slider';
import {MatDividerModule} from '@angular/material/divider';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatBadgeModule} from '@angular/material/badge';
import {MatMenuModule} from '@angular/material/menu';


import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCarouselModule } from '@ngmodule/material-carousel';
@NgModule({
  declarations: [
    AppComponent,
    HeaderMainComponent,
    FooterMainComponent,
    HomeMainComponent,
    CategoriesListComponent,
    FeaturedProductsComponent,
    OfferSliderComponent,
    SupplierStoresComponent,
    TopBrandsComponent,
    SearchBlockComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,

    MatButtonModule,
    MatSliderModule,
    FlexLayoutModule,
    MatDividerModule,
    MatIconModule,
    MatInputModule,
    MatCarouselModule,
    MatSelectModule,
    MatBadgeModule,
    MatMenuModule
    

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
