import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeMainComponent } from './pages/home-main/home-main.component';


const routes: Routes = [
  { path: '', component: HomeMainComponent },

  
  // { path: 'second-component', component: SecondComponent },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
