import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss']
})
export class CategoriesListComponent implements OnInit {

  constructor() { }

  allCategories = [
    {
      name: "Abrasives",
      count: 122,
      image: 'product-1'
    },
    {
      name: "Adhesives & Tapes",
      count: 302,
      image: 'product-2'
    },
    {
      name: "Building Materials",
      count: 122,
      image: 'product-3'
    },
    {
      name: "Cleaning & janitorial",
      count: 302,
      image: 'product-4'
    },
    {
      name: "Electrical",
      count: 302,
      image: 'product-5'
    },
    {
      name: "Electrinics & Battries",
      count: 302,
      image: 'product-6'
    },
    {
      name: "Earthing & Lighting",
      count: 122,
      image: 'product-7'
    },
    {
      name: "Fastners",
      count: 122,
      image: 'product-8'
    },
    {
      name: "Fleet & Vehicles Maintence",
      count: 122,
      image: 'product-9'
    },
    {
      name: "Furniture Hospitality & Food Services",
      count: 122,
      image: 'product-10'
    },
    {
      name: "HVAC & Refrigeration",
      count: 122,
      image: 'product-11'
    },
    {
      name: "Hardware",
      count: 122,
      image: 'product-12'
    },
    {
      name: "Hydraulics",
      count: 122,
      image: 'product-13'
    },
    {
      name: "Lab Supplies",
      count: 122,
      image: 'product-14'
    },
    {
      name: "Lighting",
      count: 302,
      image: 'product-15'
    }
  ]

  ngOnInit(): void {
  }

}
