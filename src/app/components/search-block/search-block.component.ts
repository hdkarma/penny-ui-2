import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-block',
  templateUrl: './search-block.component.html',
  styleUrls: ['./search-block.component.scss']
})
export class SearchBlockComponent implements OnInit {

  constructor() { }

  public selectedSearch='products'

  ngOnInit(): void {
  }

}
