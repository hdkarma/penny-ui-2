import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierStoresComponent } from './supplier-stores.component';

describe('SupplierStoresComponent', () => {
  let component: SupplierStoresComponent;
  let fixture: ComponentFixture<SupplierStoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SupplierStoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierStoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
