import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-supplier-stores',
  templateUrl: './supplier-stores.component.html',
  styleUrls: ['./supplier-stores.component.scss']
})
export class SupplierStoresComponent implements OnInit {

  constructor() { }

  allSuppliers = [
    {
      image: 'supplier-1',
      name: 'Sail Alreyadha Saudi Industrial Company (SIEFCO)',
      region: 'Dhahran',
    },
    {
      image: 'supplier-2',
      name: 'United Hardware Company',
      region: 'Dammam',
    },
    {
      image: '',
      name: 'Albaward tools and Hardwares',
      region: 'Dhahran',
    },
    {
      image: 'supplier-4',
      name: 'Khusheim Industrial Equipment',
      region: 'Dammam',
    },
    {
      image: '',
      name: 'Albaward tools and Hardwares',
      region: 'Jizan',
    },
    {
      image: 'supplier-6',
      name: 'Al-Rashed for Fasteners and Engineering Indus...',
      region: 'Al-Khobar',
    },
  ]


  ngOnInit(): void {
  }

}
