import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-featured-products',
  templateUrl: './featured-products.component.html',
  styleUrls: ['./featured-products.component.scss']
})
export class FeaturedProductsComponent implements OnInit {

  allFeatured = [
    {
      name: "Black, High Performance full Nitrile coated glove...",
      brand: "Dewait",
      sku: "08720-TU",
      seller_name: "Al Nojod Almotahedah",
      moq_order: "22",
      moq_rfq: "100",
      image: 'product-1',
      seller_images: ["seller-1"]
    },
    {
      name: "3M , Half Mask Respirator, 6000 Series, M, Cartr...",
      brand: "3M",
      sku: "3M-6200",
      seller_name: "Abdulrazzaq Mohammed",
      moq_order: "22",
      moq_rfq: "100",
      sar: "199.00",
      image: 'product-2',
      seller_images: ["seller-2"]
    },
    {
      name: "Yamamah Reinforcing Steel, 20mm in Diameter...",
      brand: "Yamamah Steel",
      sku: "10107-6",
      seller_name: "Anod Malmtadah",
      moq_order: "",
      moq_rfq: "100",
      image: 'product-3',
      seller_images: [''],

    },
    {
      name: "0.8/0.9 HP/KW Floor Treatment & Polishing...",
      brand: "Raimondi",
      sku: "606012",
      seller_name: "",
      moq_order: "",
      moq_rfq: "",
      image: 'product-4',
      seller_images: ["seller-1", "seller-3"],
      suppliers: 23

    },
    {
      name: "Black, High Performance full Nitrile coated glove...",
      brand: "Beybi",
      sku: "08720-TU",
      seller_name: "",
      moq_order: "",
      moq_rfq: "",
      image: '',
      seller_images: ["", ""],
      suppliers: 23

    },
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
