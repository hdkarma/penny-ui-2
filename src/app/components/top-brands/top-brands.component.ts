import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-top-brands',
  templateUrl: './top-brands.component.html',
  styleUrls: ['./top-brands.component.scss']
})
export class TopBrandsComponent implements OnInit {

  constructor() { }


  allBrands = [
    {
      image: 'brands-1',
    },
    {
      image: 'brands-2',
    },
    {
      image: 'brands-3',
    },
    {
      image: 'brands-4',
    },
    {
      image: 'brands-5',
    },
  ]
  ngOnInit(): void {
  }

}
